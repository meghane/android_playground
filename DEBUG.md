# Device Debugging

The following are the basic steps to debugging this project on a real Android device.

## General Proejct Setup
1. Setup ssh key and link to bitbucket
2. Clone android playground
3. Import android playground in Android Studio 

## Device connection and install
1. Connect phone to computer in `file transfers` mode
2. Enable `Developer Options`, if not enabled already
        
        In system settings, select `About phone`. 
        Tap `Build number` multiple times until `developer options` are enabled.
        
3. Enable `Stay awake` in developer options
4. Enable `USB debugging` in developer options
5. Install app:

        $ ./gradlew installDebug

### Version
1.0.0
