# README #

### Environment Details ###

* Java 7
* Android 5.0 Lollipop - 6.0 Marshmallow
* [Android Studio](https://developer.android.com/sdk/index.html)

### Android Tests ###

* Activity Lifecycle
* SingleTouch Events
* MultiTouch Events
* RenderViews
* Bitmaps
* OpenGL ES (Maybe?)
* MultiThreaded SurfaceViews

### Build ###
* You should be able to clone this down, import into Android Studio or Intellij, run gradle build and go.

### Debug ###
[Debugging](DEBUG.md)

