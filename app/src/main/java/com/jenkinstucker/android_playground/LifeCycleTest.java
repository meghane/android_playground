package com.jenkinstucker.android_playground;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by beezuz on 11/21/15.
 */
public class LifeCycleTest extends Activity {

    /**
     * Notes:
     * onPause()
     * The Activity can be destroyed silently anytime after onPause().
     * We should never assume onStop() or onDestroy() gets called.
     * <p/>
     * Activity.isFinishing() lets us know if the system is going to kill
     * the Activity. Place within onPause(), and if it returns true, persist everything
     * <p/>
     * onCreate(), onStart()
     * These are both transient methods chained to onResume()
     * onResume() is the only method that signals to us we are actually transitioning to
     * a running state. It is in here that we'll start our game loop thread.
     * <p/>
     * onCreate() is where we'll setup graphical components needed in the UI
     */

    StringBuilder builder = new StringBuilder();
    TextView txtView;
    private String tag = "LIFECYCYLE_TEST";

    private void log(String text) {
        Log.d(tag, text);
        builder.append(text).append('\n');
        txtView.setText(builder.toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        txtView = new TextView(this);
        txtView.setText(builder.toString());
        setContentView(txtView);
        log("created");
    }

    @Override
    protected void onResume() {
        super.onResume();
        log("resumed");
    }

    @Override
    protected void onPause() {
        super.onPause();
        log("paused");

        if (isFinishing()) {
            //persist object state!
            log("finishing");
        }
    }
}
