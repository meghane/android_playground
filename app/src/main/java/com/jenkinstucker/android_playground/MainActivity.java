package com.jenkinstucker.android_playground;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends ListActivity {

    String tests[] = {"LifeCycleTest", "SingleTouchTest", "MultiTouchTest",
            "KeyTest", "AssetsTest", "SoundPoolTest", "FullScreenTest", "RenderViewTest",
            "ShapeTest", "BitmapTest", "SurfaceViewTest"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setListAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, tests));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        String testName = tests[position];

        try {
            Class clazz = Class.forName("com.jenkinstucker.android_playground." + testName);
            Intent intent = new Intent(this, clazz);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
