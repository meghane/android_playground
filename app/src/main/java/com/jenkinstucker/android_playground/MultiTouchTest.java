package com.jenkinstucker.android_playground;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

/**
 * Created by beezuz on 11/21/15.
 */
@TargetApi(23)
public class MultiTouchTest extends Activity implements OnTouchListener {

    /**
     * Notes:
     * Still implement OnTouchListener interface, like SingleTouch Events,
     * and still get MotionEvent instances and
     * many same EventTypes like MotionEvent.ACTION_UP, etc . . .
     * <p/>
     * Overloaded MotionEvent.getX(pointerIndex), MotionEvent.getY(pointerIndex)
     * <p/>
     * PointerIndex is an index into internal arrays of a
     * MotionEvent instance which holds coordinates of a finger on the screen.
     * <p/>
     * Identifier of a finger on the screen is the PointerIdentifier, which is a
     * number that uniquely identifies one instance of a pointer on the screen.
     * MotionEvent.getPointerIdentifier(int pointerIndex)
     * <p/>
     * Obtaining PointerIndex: (Yikes)
     * int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >>
     * MotionEvent.ACTION_POINTER_INDEX_SHIFT;
     *
     * @see <a href="http://developer.android.com/reference/android/view/MotionEvent.html">Documention</a>
     * <p/>
     * Quick Bitshift Examples:
     * 00000001 << 1 = 00000010
     * <p/>
     * A binary number that begins with '1' will shift in '1's.
     * A binary number that begins with '0' will shift in '0's.
     * Starts in 1's: 11111111 11111111 11111111 11110000 >> 4 = 11111111 11111111 11111111 11111111
     * Starts in 0's: 00001111 11111111 11111111 11111111 >> 4 = 00000000 11111111 11111111 11111111
     * <p/>
     * "Unsigned shift right" operator: >>> always shifts in a "0" regardless of the sign
     * 11111111 11111111 11111111 11110000 >>> 4 = 00001111 11111111 11111111 11111111
     * <p/>
     * & -> Bitwise AND  example: 1010 & 0101 = 0000, 1100 & 1000 = 1000, etc . . .
     * <p/>
     * MotionEvent.ACTION_POINTER_DOWN - Happens after additional finger touches screen after first finger
     * MotionEvent.ACTION_POINTER_UP - anagalous to above.
     * Last finger to lift off screen fires MotionEvent.ACTION_UP event
     */

    StringBuilder builder = new StringBuilder();
    TextView view;

    //capture up to 10 fingers
    float[] x = new float[10];
    float[] y = new float[10];
    boolean[] touched = new boolean[10];
    int[] id = new int[10];

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = new TextView(this);
        view.setText("Multitouch and drag - multiple fingers");
        view.setOnTouchListener(this);
        setContentView(view);

        for (int i = 0; i < 10; i++) {
            id[i] = -1;
        }

        updateTextView(-1);
    }

    private void updateTextView(int pointerIndex) {
        builder.setLength(0);

        for (int i = 0; i < 10; i++) {
            builder.append("Touched: ").append(touched[i]);
            builder.append(", ID: ").append(id[i]);
            builder.append(", X: ").append(x[i]);
            builder.append(", Y: ").append(y[i]);
            builder.append(", WTF is a PointerIndex, Really?: ").append(pointerIndex);
            builder.append('\n');
        }
        view.setText(builder.toString());
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        //MotionEvent.ACTION_MASK - Bit mask of the parts of the action code that are the action itself.
        int action = motionEvent.getAction() & MotionEvent.ACTION_MASK;

        int pointerIndex = (motionEvent.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >>
                MotionEvent.ACTION_POINTER_INDEX_SHIFT;

        int pointerCount = motionEvent.getPointerCount();

        for (int i = 0; i < 10; i++) {
            if (i >= pointerCount) {
                touched[i] = false;
                id[i] = -1;
                continue;
            }

            int pointerId = motionEvent.getPointerId(i);

            switch (action) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    touched[i] = true;
                    id[i] = pointerId;
                    x[i] = (int) motionEvent.getX(i);
                    y[i] = (int) motionEvent.getY(i);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_OUTSIDE:
                    touched[i] = false;
                    id[i] = -1;
                    x[i] = (int) motionEvent.getX(i);
                    y[i] = (int) motionEvent.getY(i);
                    break;
                case MotionEvent.ACTION_MOVE:
                    touched[i] = true;
                    id[i] = pointerId;
                    x[i] = (int) motionEvent.getX(i);
                    y[i] = (int) motionEvent.getY(i);
                    break;
            }
        }
        updateTextView(pointerIndex);
        return true;
    }
}
