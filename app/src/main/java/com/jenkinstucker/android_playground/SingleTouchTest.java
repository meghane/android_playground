package com.jenkinstucker.android_playground;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

/**
 * Created by beezuz on 11/21/15.
 */
public class SingleTouchTest extends Activity implements OnTouchListener {

    /**
     * Notes:
     * Touch Events are passed to an onTouchListener interface implementation
     * that we register with the view
     * <p/>
     * The OnTouchListener interface has only a single method:
     * public abstract boolean onTouch(View v, MotionEvent event)
     * <p/>
     * Note within the onCreate() method - setOnTouchListener(this)
     * <p/>
     * Three relevant methods:
     * MotionEvent.getAction()
     * MotionEvent.getX(), MotionEvent.getY()
     */

    StringBuilder builder = new StringBuilder();
    TextView view;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = new TextView(this);
        view.setText("Touch and drag - one finger only");
        view.setOnTouchListener(this);
        setContentView(view);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        builder.setLength(0);

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                builder.append("pressing -- ACTION_DOWN . . . ");
                break;
            case MotionEvent.ACTION_MOVE:
                builder.append("dragging -- ACTION_MOVE . . . ");
                break;
            case MotionEvent.ACTION_CANCEL:
                builder.append("not sure -- ACTION_CANCEL");
                break;
            case MotionEvent.ACTION_UP:
                builder.append("finger up -- ACTION_UP");
                break;
        }

        builder.append("  X: ").append(motionEvent.getX());
        builder.append(",  Y: ").append(motionEvent.getY());

        String text = builder.toString();
        Log.d("TouchTest ", text);
        this.view.setText(text);
        return true;
    }
}
